
public class Enregistrement { //Classe permettant de generaliser certaines 
							 // methodes utilisees avec differentes entitees
	private String numero;

	public String getNumero() {
		return numero;
	}

    public void setNumero(String numero) {
		this.numero = numero;
	}
}
