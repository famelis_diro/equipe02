
public class GererConfirmation extends Gestion {

	public static void confirmer() {  						//methode pour confirmer une inscription
		
			String numeroMembre = ""; 						// numero du membre voulant confirmer une inscription initialise a la chaine vide
			Seance[] seances = GererSeance.getSeanceList(); //retourne la liste des seances sous forme de tableau
			if(seances != null) { 						 	//Si la liste est vide (le fichier contenant la liste est vide ou n'existe pas)
					
				for(int i = 0; i< seances.length ; i++) // Affiche les informations des sceances
				{
					System.out.println(seances[i].afficherInfo(false));  
				}
				System.out.println();
				
				String numeroSeance = Menu.validNum("Entrez le numero de la seance voulue : ", 7); //Lit le numero de seance entre par l'utilisateur 
				if(!numeroSeance.equals("QUIT")) {   							//Si l'utilisateur ne desire pas quitter
					int index = Utils.chercherChose(numeroSeance,seances); //Trouve l'index de la seance voulu dans la liste des sceance
					if(index != -1) { 									//Si la seance est trouvee, afficher ses informations
						System.out.println();
						System.out.println(seances[index].getNumero());
						System.out.println(seances[index].getDateDebut());
						System.out.println(seances[index].getDateFin());
						System.out.println(seances[index].getHeure());
						System.out.println(seances[index].getCapacite());
						System.out.println(seances[index].getFraisDeService());
						System.out.println(seances[index].getCommentaires());
						if(seances[index].isLundi()) {System.out.println("Lundi");}
						if(seances[index].isMardi()) {System.out.println("Mardi");}
						if(seances[index].isMercredi()) {System.out.println("Mercredi");}
						if(seances[index].isJeudi()) {System.out.println("Jeudi");}
						if(seances[index].isVendredi()) {System.out.println("Vendredi");}
						if(seances[index].isSamedi()) {System.out.println("Samedi");}
						if(seances[index].isDimanche()) {System.out.println("Dimanche");}
						System.out.println();
					
					//L'utilisateur peut appuyer sur r pour confirmer une inscription
					System.out.print("Appuyer sur r confirmer une inscription ");
					String r = Utils.lireData();
					if (r.equals("r")) {
						numeroMembre = GererUtilisateur.acceder("Confirmation","e"); //Confirme que le numero du membre est valide
						if(!numeroMembre.equals("refus")) {
							Inscription[] inscriptions = getInscriptionList(); //retourne la liste des inscriptions sous forme de tableau
							if(inscriptions == null) {  //Si la liste est vide (le fichier contenant la liste est vide ou n'existe pas)
														
								System.out.println("Inscription non trouvee");
							} else {
								boolean trouve = false;
								for(Inscription i : inscriptions) { //On cherche l'inscription dans la liste d'inscriptions
									if(numeroSeance.equals(i.getNumero())) {
										if(numeroMembre.equals(i.getNumeroMembre())){
											trouve = true;
											break;
										}
									}
								}
								if(trouve) { // Si on trouve l'inscription
									Confirmation conf = new Confirmation(); // On cree la confirmation
									conf.setNumero(numeroSeance);
									conf.setNumeroMembre(numeroMembre);
									conf.setNumeroProfessionnel(seances[index].getNumeroProfessionnel());
									conf.setDateEtHeureActuelle(Utils.getDateHeure());
									System.out.println("Entrer des commentaires (facultatif): ");
									String Commentaires = Utils.lireData();
									conf.setCommentaires(Commentaires);
								
									
									Confirmation[] confirmations = getConfirmationList(); //retourne la liste des seances sous forme de tableau
									
									if(confirmations != null) {     //Si la liste est vide (le fichier contenant la liste est vide ou n'existe pas)
										
										confirmations = (Confirmation[]) ajouterTableauConfirmation(confirmations,conf);  //On actualise la liste des confirmations
										Utils.ecrireTableauChose(confirmations,FILE_CONFIRMATIONS,false); //On ecrit la nouvelle liste dans le fichier Confirmations.txt
									}
									else {Confirmation[] confirmations2 = new Confirmation[1]; // On cree une liste de confirmations
											confirmations2[0] = conf;
											Utils.ecrireTableauChose(confirmations2,FILE_CONFIRMATIONS,false); //On ecrit la liste dans le fichier Confirmations.txt
											}
									System.out.println();
									System.out.println("Confirmation confirmee");
									
								} else {
									System.out.println("Inscription non trouvee");
								}
							}
						}
					}
				}else {
					System.out.println();
					System.out.println("Numero de seance invalide");
				}
			}	
		} else {
				System.out.println();
				System.out.println("Aucune seance");
				}
			if(!numeroMembre.equals("refus")) {
				System.out.println();
				System.out.println("Appuyer sur entrer pour continuer");
				Utils.lireData();
			}
	}
	//Methode pour ajouter a un tableau de type Confirmation un element supplementaire de type Confirmation
	public static Confirmation[] ajouterTableauConfirmation(Confirmation[] tab, Confirmation val) {
		int length = tab.length;
		Confirmation[] ret = new Confirmation[length+1];
		for(int i=0;i<length; i++) {ret[i] = tab[i];}
		ret[length] = val;
		return ret;
	}

}
