
public class GererInscription extends Gestion {

	public static void inscrire() {								//methode pour faire une inscription
		String numeroMembre = GererUtilisateur.acceder("Inscription","e"); //Confirme que le numero du membre est valide
		
		if(!numeroMembre.equals("QUIT")) { // Si l'utilisateur desire quitter
			if(!numeroMembre.equals("refus")) { // Si le numero est valide
				Seance[] seances = getSeanceList(); //retourne la liste des seances sous forme de tableau
				if(seances != null) { //Si la liste est vide (le fichier contenant la liste est vide ou n'existe pas)
				for(int i = 0; i< seances.length ; i++)
				{
					System.out.println(seances[i].afficherInfo(false)); // Affiche les informations des sceances
					
				}
				System.out.println();
			
				String numeroSeance = Menu.validNum("Entrez le numero de la seance voulue : ", 7); //Lit le numero de seance entre par l'utilisateur
				if(!numeroSeance.equals("QUIT")) { 							// Si l'utilisateur ne desire pas quitter
					int index = Utils.chercherChose(numeroSeance,seances);  //Trouve l'index de la seance voulu dans la liste des sceance
					if (index != -1) { 										//Si la seance est trouvee, on cree l'inscription
						Inscription[] inscriptions = getInscriptionList();  //retourne la liste des inscriptions sous forme de tableau
						Inscription ins = new Inscription();
						ins.setNumero(numeroSeance);
						ins.setNumeroMembre(numeroMembre);
						ins.setNumeroProfessionnel(seances[index].getNumeroProfessionnel());
						ins.setDateEtHeureActuelle(Utils.getDateHeure());
						System.out.print("Entrer la date de la seance au format JJ-MM-AAAA : ");
						String date = Utils.lireData(); 
						ins.setDate(date);
						System.out.print("Entrer des commentaires (facultatif) : ");
						String Commentaires = Utils.lireData(); // a changer(verifier pas trop  de caracteres)
						ins.setCommentaires(Commentaires);
					
					
						if(inscriptions != null) { //Si la liste d'inscriptions est vide (le fichier contenant la liste est vide ou n'existe pas)
							inscriptions = (Inscription[]) ajouterTableauInscription(inscriptions,ins); //On actualise la liste des inscriptions
							Utils.ecrireTableauChose(inscriptions,FILE_INSCRIPTIONS,false); //On ecrit la nouvelle liste dans le fichier Inscriptions.txt
						}
						else {Inscription[] inscriptions2 = new Inscription[1];				//On cree la liste d'inscriptions
								inscriptions2[0] = ins;
								Utils.ecrireTableauChose(inscriptions2,FILE_INSCRIPTIONS,false); //On ecrit la liste dans le fichier Inscriptions.txt
								}
						System.out.println();
						System.out.println("Inscription confirmee");
						System.out.println();
						System.out.println("Frais a payer : " + seances[index].getFraisDeService());
					} else {
						System.out.println();	
						System.out.println("Numero de seance invalide");
						
					
					
					}
					System.out.println();
					System.out.println("Appuyer sur entrer pour continuer");
					Utils.lireData();}
				
			} else {
				System.out.println("Aucune seance");
				System.out.println();
				System.out.println("Appuyer sur entrer pour continuer");
				Utils.lireData();
			}
			}
		}
		
	}
	
	//Methode pour ajouter a un tableau de type Inscription un element supplementaire de type Inscription
	public static Inscription[] ajouterTableauInscription(Inscription[] tab, Inscription val) {
		int length = tab.length;
		Inscription[] ret = new Inscription[length+1];
		for(int i=0;i<length; i++) {ret[i] = tab[i];}
		ret[length] = val;
		return ret;
	}
}
