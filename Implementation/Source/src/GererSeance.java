
public class GererSeance extends Gestion {
	protected static final int SEANCE_LENGTH = 7;
	protected static final int UTILISATEUR_LENGTH = 9;
	
	protected static final String PAS_PROFESSIONNEL = "Cet utilisateur n'est pas un professionnel";
	private static final String ADD_TITLE = "----Creer une seance----";
	private static final String MOD_TITLE = "----Modifier une seance----";
	private static final String MOD_FIN = "----La modification a ete faite avec succes----";
	private static final String DEL_FIN = "----La suppression a ete faite avec succes----";
	private static final String ADD_FIN = "----La creation a ete faite avec succes----";
	
	static void ajouter() { 			//methode pour creer une seance
		System.out.println();
		System.out.println(ADD_TITLE);
		System.out.println();
		
		Seance[] seances = getSeanceList(); //retourne la liste des seances sous forme de tableau
		
			Utilisateur[] users = getUserList(); //retourne la liste des utilisateurs sous forme de tableau
			if (users != null) {				//Si la liste des utilisateurs est vide (le fichier contenant la liste est vide ou n'existe pas)
				String id = MenuSeance.validNum(Menu.USER_PROMPT,UTILISATEUR_LENGTH); //Lit le numero du professionnel entre par l'utilisateur 
				if(!id.equals("QUIT")) {
				
					int index = Utils.chercherChose(id,users);  //Trouve l'index de l'utilisateur voulu dans la liste des utilisateurs
					
					if(index!=-1 && users[index].isProfessionnel()) { //Si l'utilisateur est trouve et qu'il s'agit bel et bien d'un professionnel
						Seance s = MenuSeance.ajouter(id);            //On cree une nouvelle seance
						if(seances != null) {							//Si la liste des seances est vide (le fichier contenant la liste est vide ou n'existe pas)
							s.setNumero(Utils.genererNumeroEnregistrement(seances, SEANCE_LENGTH)); //On genere un nouveau numero de seance
							seances = (Seance[]) ajouterTableauSeance(seances,s);					//On actualise la liste des seances
							Utils.ecrireTableauChose(seances,FILE_SEANCE,true);						//On ecrit la nouvelle liste dans le fichier Seances.txt
						} else {
							s.setNumero("0000000");													//On Cree la liste des seances
							Seance[] seances2 = new Seance[1];										
							seances2[0] = s;
							Utils.ecrireTableauChose(seances2,FILE_SEANCE,true);					//On ecrit la liste dans le fichier Seances.txt
							
							
						}
						System.out.println();
						System.out.println(ADD_FIN);
						System.out.println();
					}
					else {System.out.println();
						System.out.println(PAS_PROFESSIONNEL);}
					
					
				}
			} else {System.out.println("Aucun utilisateur");
			}
			System.out.println();
			System.out.println("Appuyer sur entrer pour continuer");
			Utils.lireData();
		
	}
		
	static void modifier() {									//methode pour modifier une seance
		System.out.println();
		System.out.println(MOD_TITLE);
		System.out.println();
			Seance[] seances = getSeanceList();						//retourne la liste des seances sous forme de tableau
			if(seances != null) {									//Si la liste des seances est vide (le fichier contenant la liste est vide ou n'existe pas)
				Utilisateur[] users = GererUtilisateur.getUserList();//retourne la liste des utilisateurs sous forme de tableau
				if (users != null) {								//Si la liste des utilisateurss est vide (le fichier contenant la liste est vide ou n'existe pas)
					String idUser = MenuSeance.validNum(Menu.USER_PROMPT,UTILISATEUR_LENGTH); //Lit le numero du professionnel entre par l'utilisateur
					int indexUser = Utils.chercherChose(idUser,users);						//Trouve l'index de l'utilisateur voulu dans la liste des utilisateurs
					
					if(indexUser !=-1 && users[indexUser].isProfessionnel())				//Si l'utilisateur est trouve et qu'il s'agit bel et bien d'un professionnel
					{																		
						String idSeance = MenuSeance.validNum(Menu.SEANCE_PROMPT,SEANCE_LENGTH);  //Lit le numero de la seance entre par l'utilisateur
						int indexSeance = Utils.chercherChose(idSeance,seances);					//Trouve l'index de la seance voulu dans la liste des seances
						
						if(indexSeance != -1 && users[indexUser].getNumero().equals(seances[indexSeance].getNumeroProfessionnel())) { //Si la seance voulu set dans la liste des seances et est bel et bien une seance de notre professionnel
							seances[indexSeance] = MenuSeance.modifier(seances[indexSeance]);										//On actualise la liste des seances
							Utils.ecrireTableauChose(seances,FILE_SEANCE,true);														//On ecrit la liste actualise dans le fichier Seances.txt
					
							System.out.println();
							System.out.println(MOD_FIN);
							System.out.println();
						}
						
						else {
							System.out.println();
							System.out.println(INVALID_CHOICE);
						}		
					}
					else {
						System.out.println();
						System.out.println(PAS_PROFESSIONNEL);
					}
					
				}else {
					System.out.println();
					System.out.println("Aucun utilisateur");
				}
			}else {System.out.println("Aucune seance");}
			System.out.println();
			System.out.println("Appuyer sur entrer pour continuer");
			Utils.lireData();
	}
	
	static void supprimer() {  //methode pour supprimer une seance
		Seance[] seances = getSeanceList(); 
		if(seances != null) {
			Utilisateur[] users = GererUtilisateur.getUserList(); //retourne la liste des utilisateurs sous forme de tableau
			if (users != null) {
				String idUser = MenuSeance.validNum(Menu.USER_PROMPT,UTILISATEUR_LENGTH); // valide le numero de l'utilisateur et le valide
				int indexUser = Utils.chercherChose(idUser,users);
				
				if(indexUser !=-1 && users[indexUser].isProfessionnel()) //si l'utilisateur a ete trouve 
				{
					String idSeance = MenuSeance.validNum(Menu.SEANCE_PROMPT,SEANCE_LENGTH); // lit et valide le numero de la seance
					int indexSeance = Utils.chercherChose(idSeance,seances);
					if(indexSeance != -1 && users[indexUser].getNumero().equals(seances[indexSeance].getNumeroProfessionnel())) { //verifie si la seance est offerte pas le professionnel lu plus tot
						seances = (Seance[])Utils.retirerChose(indexSeance,seances); //retire la seance de la liste
						Utils.ecrireTableauChose(seances,FILE_SEANCE,true); // ecrit le tebleau dans le fichier texte
						
						System.out.println();
						System.out.println(DEL_FIN);
						System.out.println();
					}
					else {
						System.out.println();
						System.out.println(INVALID_CHOICE);

					}		
				}
				else {
					System.out.println();
					System.out.println(PAS_PROFESSIONNEL);
				}

			
			}else {
				System.out.println();
				System.out.println("Aucun utilisateur");}
		
		}else {
			System.out.println();
			System.out.println("Aucune seance");
		}
		System.out.println();
		System.out.println("Appuyer sur entrer pour continuer");
		Utils.lireData();
	
	}
	
	static void consulter() { // permet de consulter une seance de service 
		
		
		Utilisateur[] utilisateurs = getUserList(); 
		if(utilisateurs != null) {
		String numeroProfessionnel = MenuUtilisateur.validNum(Menu.USER_PROMPT,UTILISATEUR_LENGTH); // lit et valide le numero du professionnel
		int index = Utils.chercherChose(numeroProfessionnel,utilisateurs); // verifie si le numero de professionnel est dans la liste d'utilisateur
		if(index != -1) {
		if(utilisateurs[index].isProfessionnel()) { // si l'utilisateur est un professionnel
			Seance[] listeSeance = getSeanceList();
			if(listeSeance != null) {
			int index2 = 0;
			for(Seance i : listeSeance) {
				if(i.getNumeroProfessionnel().equals(numeroProfessionnel)) { // verifie si le professionnel est l'auteur de la seance 
					index2++;
				}	
			}
			if(index2 != 0) {
				String[][] tableau = new String[index2][2]; 
				int j = 0;
				for(Seance i : listeSeance) {
					if(i.getNumeroProfessionnel().equals(numeroProfessionnel)) {
						tableau[j][0] = i.getNumero();
						j++;
					}	
				}
				for(int k = 0; k < index2;k++ ) {
					
					tableau[k][1] = "0";
				
				}
				Inscription[] listeInscription = getInscriptionList();        
				if(listeInscription != null) {
				for(Inscription i : listeInscription) {
					if(i.getNumeroProfessionnel().equals(numeroProfessionnel)) {
						for(int k = 0; k < index2;k++ ) {
							if(tableau[k][0].equals(i.getNumero())) {
								String nbreInscritsString = tableau[k][1];
								int nbreInscritsInt = Utils.stringToInt(nbreInscritsString);
								nbreInscritsInt++;
								nbreInscritsString = String.valueOf(nbreInscritsInt);
									tableau[k][1] = nbreInscritsString ;
							}
						}
					}	
				}
				
			}
				System.out.println();
				for(int k = 0; k < index2;k++ ) {
					
				System.out.println(tableau[k][0] + "        " + tableau[k][1]);
				
				}
			} else {
				System.out.println("Aucune seances donnees");
			}
			} else {System.out.println("Aucune seance");}
		} else {
			System.out.println(PAS_PROFESSIONNEL);
		}
		}else {
			System.out.println();
			System.out.println("numero d'utilisateur non valide");
			}
		}else {System.out.println("Aucun Utilisateur");}
		System.out.println();
		System.out.println("Appuyer sur entre pour continuer");
		Utils.lireData();
		
}
	//Methode pour ajouter a un tableau de type Confirmation un element supplementaire de type Confirmation
	public static Seance[] ajouterTableauSeance(Seance[] tab, Seance val) {
		int length = tab.length;
		Seance[] ret = new Seance[length+1];
		for(int i=0;i<length; i++) {ret[i] = tab[i];}
		ret[length] = val;
		return ret;
	}
	

}
