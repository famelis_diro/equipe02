
public class GererUtilisateur extends Gestion
{
	private static final String NUMERO_DEFAULT0 = "000000000";
	private static final String NUMERO_DEFAULT1 = "000000001";
	
	static void ajouter() { // permet d'ajouter un utilisateur
		Utilisateur[] users = getUserList(); 
		Utilisateur u = MenuUtilisateur.ajouter();
		
		if(u.isPaye() && users!=null) {u.setNumero(Utils.genererNumeroEnregistrement(users,UTILISATEUR_LENGTH)); // genere un numero de membre si celui ci a paye
		users = (Utilisateur[]) ajouterTableauUtilisateur(users,u);
		Utils.ecrireTableauChose(users,FILE_MEMBRES,true);}
		else if (!u.isPaye() && users!=null){
		u.setNumero(NUMERO_DEFAULT0);
		users = (Utilisateur[]) ajouterTableauUtilisateur(users,u);
		Utils.ecrireTableauChose(users,FILE_MEMBRES,true);
		}
		else {
			u.setNumero(NUMERO_DEFAULT1);				//si le fichier n'existe pas 
			Utilisateur[] users1 = new Utilisateur[1];
			users1[0]=u;
			Utils.ecrireTableauChose(users1,FILE_MEMBRES,true);
			}
		if(!u.getNumero().equals(NUMERO_DEFAULT0)) {
			System.out.println();
			System.out.println("Le numero de "
				+ "l'utilisateur est : " + u.getNumero());} else {/*...*/}
			System.out.println();
			System.out.println("Appuyer sur entrer pour continuer");
			Utils.lireData();
		
		
		
		
	}

	static void modifier() { 
		Utilisateur[] users = getUserList();
		if(users != null) { // si le fichier existe
			String id = MenuUtilisateur.validNum(Menu.USER_PROMPT,UTILISATEUR_LENGTH);
			int index = Utils.chercherChose(id,users);
			if(!id.equals("QUIT")) { // si l'agent quitte
				if(index == -1) {System.out.println(INVALID_CHOICE);}
				else {
					users[index] = MenuUtilisateur.modifier(users[index]);
					Utils.ecrireTableauChose(users,FILE_MEMBRES,true);
					System.out.println();
					System.out.println("Modifications confirmees");
				}	
			}
		}else {
			System.out.println("Aucun utilisateur");
		}
		System.out.println();
		System.out.println("Appuyer sur entrer pour continuer");
		Utils.lireData();
	}

	static void supprimer() { //sensiblement la meme chose que modifier
		Utilisateur[] users = getUserList();
		if(users != null) {
			String id = MenuUtilisateur.validNum(Menu.USER_PROMPT,UTILISATEUR_LENGTH);
			if(!id.equals("QUIT")) {
				int index = Utils.chercherChose(id,users);
	
				if(index == -1) {System.out.println(INVALID_CHOICE);}
				else {
					users = (Utilisateur[])Utils.retirerChose(index,users);
					Utils.ecrireTableauChose(users,FILE_MEMBRES,true);
					System.out.println();
					System.out.println("Suppression confirmee");
				}
			}
		} else {
			System.out.println("Aucun utilisateur");
		}
		System.out.println();
		System.out.println("Appuyer sur entrer pour continuer");
		Utils.lireData();
	}
	
	public static Utilisateur[] ajouterTableauUtilisateur(Utilisateur[] tab, Utilisateur val) { // ajoute un element au tableaud'utilisateur
		int length = tab.length;
		Utilisateur[] ret = new Utilisateur[length+1];
		for(int i=0;i<length; i++) {ret[i] = tab[i];}
		ret[length] = val;
		return ret;
	}

	public static String acceder(String msg1, String msg2) {// msg2 = feminin ou masculin 
		Utilisateur[] users = getUserList();
		if(users != null) {
			System.out.println();
			String id = MenuUtilisateur.validNum(Menu.USER_PROMPT,UTILISATEUR_LENGTH);
			if(id.equals("QUIT")) {return id;}
			System.out.println();
			int index = Utils.chercherChose(id,users);
			if((index == -1)) {
				id = "refus";
				System.out.println(msg1 + " refuse" + msg2 + " (numero de membre invalide)");}
			else if(users[index].isPaye() == false) {
				id = "refus";
				System.out.println(msg1 + " refuse" + msg2 + " (Membre banni)");}
			else {
				System.out.println(msg1 + " permis" + msg2);					
			}
			System.out.println();
			System.out.println("Appuyer sur entrer pour continuer");
			Utils.lireData();
			return id;
		} else { 
			System.out.println();
			System.out.println("Aucun membre");
			System.out.println();
			System.out.println("Appuyer sur entrer pour continuer");
			Utils.lireData();
			return "refus";}
	}
}
