import java.util.Arrays;

public class Gestion { //contient les methodes pour recuperer les tableau d'utilisateurs,seances,enregistrement et inscriptions
	
	protected static String FILE_SEANCE = "Seances.txt";
	protected static String FILE_MEMBRES = "Utilisateurs.txt";
	protected static String FILE_INSCRIPTIONS = "Inscriptions.txt";
	protected static String FILE_CONFIRMATIONS= "Confirmations.txt";
	
	protected static final String INVALID_CHOICE = "Choix invalide";
	
	protected static final int UTILISATEUR_LENGTH = 9;
	
	static Utilisateur[] getUserList() {
		String[][] fichier = Utils.fichier2Tableau(FILE_MEMBRES); //lit le fichier texte
		if(fichier == null) {return null;}
		int length = fichier.length;
		Utilisateur[] u = new Utilisateur[length];
		for(int i = 0; i < length;i++) {
			int j = 0;
			u[i]= new Utilisateur();//affecte les valeurs du fichier au bon atributs
			u[i].setNumero(fichier[i][j++]);
			u[i].setPrenom(fichier[i][j++]);
			u[i].setNom(fichier[i][j++]);
			u[i].setPaye(Boolean.valueOf(fichier[i][j++]));
			u[i].setProfessionnel(Boolean.valueOf(fichier[i][j++]));
		}
		Arrays.sort(u);
		return u;
	}
	
	static Seance[] getSeanceList() {
		String[][] fichier = Utils.fichier2Tableau(FILE_SEANCE);
		if(fichier == null) {return null;}
		int length = fichier.length;
		Seance[] u = new Seance[length];
		for(int i = 0; i < length;i++) {
			int j = 0;
			u[i]= new Seance();
			u[i].setNumero(fichier[i][j++]);
			u[i].setNumeroProfessionnel(fichier[i][j++]);
			u[i].setDateEtHeureActuelle(fichier[i][j++]);
			u[i].setDateDebut(fichier[i][j++]);
			u[i].setDateFin(fichier[i][j++]);
			u[i].setHeure(fichier[i][j++]);
			u[i].setCapacite(fichier[i][j++]);
			u[i].setFraisDeService(fichier[i][j++]);
			u[i].setCommentaires(fichier[i][j++]);
			u[i].setLundi(Boolean.valueOf(fichier[i][j++]));
			u[i].setMardi(Boolean.valueOf(fichier[i][j++]));
			u[i].setMercredi(Boolean.valueOf(fichier[i][j++]));
			u[i].setJeudi(Boolean.valueOf(fichier[i][j++]));
			u[i].setVendredi(Boolean.valueOf(fichier[i][j++]));
			u[i].setSamedi(Boolean.valueOf(fichier[i][j++]));
			u[i].setDimanche(Boolean.valueOf(fichier[i][j++]));
		}
		return u;
	}
	
	static Confirmation[] getConfirmationList() {
		String[][] fichier = Utils.fichier2Tableau(FILE_CONFIRMATIONS);
		if(fichier == null) {return null;}
		int length = fichier.length;
		Confirmation[] u = new Confirmation[length];
		for(int i = 0; i < length;i++) {
			int j = 0;
			u[i]= new Confirmation();
			u[i].setNumero(fichier[i][j++]);
			u[i].setNumeroMembre(fichier[i][j++]);
			u[i].setNumeroProfessionnel(fichier[i][j++]);
			u[i].setDateEtHeureActuelle(fichier[i][j++]);
			u[i].setCommentaires(fichier[i][j++]);
			
		}
		return u;
	}
	
	static Inscription[] getInscriptionList() {
		String[][] fichier = Utils.fichier2Tableau(FILE_INSCRIPTIONS);
		if(fichier == null) {return null;}
		int length = fichier.length;
		Inscription[] u = new Inscription[length];
		for(int i = 0; i < length;i++) {
			int j = 0;
			u[i]= new Inscription();
			u[i].setNumero(fichier[i][j++]);
			u[i].setNumeroMembre(fichier[i][j++]);
			u[i].setNumeroProfessionnel(fichier[i][j++]);
			u[i].setDateEtHeureActuelle(fichier[i][j++]);
			u[i].setDate(fichier[i][j++]);
			u[i].setCommentaires(fichier[i][j++]);
			
		}
		return u;
	}

}
