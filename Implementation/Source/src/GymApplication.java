
public class GymApplication { // Point d'entrer du programme
	
	public static void main(String args[]) {
		Utils.ouvrirScanner();
		LaunchMenu.afficherMenu();  // Afficher le menu principal
		Utils.fermerBufferedReader();
		Utils.fermerScanner();
	}
}