
public class Inscription extends Enregistrement  {

private static final char DELIMITER = '~';
	
	
	private String numero;
	private String numeroMembre;
	private String numeroProfessionnel;
	private String dateEtHeureActuelle;
	private String date;
	private String commentaires;

	
	@Override
	public String toString() {
		return numero + DELIMITER + numeroMembre + DELIMITER + numeroProfessionnel + DELIMITER
				+ dateEtHeureActuelle + DELIMITER + date + DELIMITER + commentaires + DELIMITER ;
	}
	
	@Override
    public boolean equals(Object o) { 
		String s = (String) o;
        return numero.equals(s);
    }
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNumeroMembre() {
		return numeroMembre;
	}

	public void setNumeroMembre(String numeroMembre) {
		this.numeroMembre = numeroMembre;
	}

	public String getNumeroProfessionnel() {
		return numeroProfessionnel;
	}

	public void setNumeroProfessionnel(String numeroProfessionnel) {
		this.numeroProfessionnel = numeroProfessionnel;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDateEtHeureActuelle() {
		return dateEtHeureActuelle;
	}

	public void setDateEtHeureActuelle(String dateEtHeureActuelle) {
		this.dateEtHeureActuelle = dateEtHeureActuelle;
	}

	public String getCommentaires() {
		return commentaires;
	}

	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}

}
