
public class LaunchMenu {
	private static final String ADD_TITLE = "----Ajouter un utilisateur----";
	private static final String MOD_TITLE = "----Modifier le dossier d'un utilisateur----";
	private static final String DEL_TITLE = "----Supprimer le dossier d'un utilisateur----";
	
	
	
	public static void afficherMenu() { // gere les menu  
		MenuMain.splash();
		for(;;) {
			switch(MenuMain.afficher()) {
			
			case 1:
				System.out.println("Acces au centre");
				GererUtilisateur.acceder("Acces", "");
				break;
			case 2:
				switch(MenuUtilisateur.main()){
				
				case 1:
					System.out.println();
					System.out.println(ADD_TITLE);
					System.out.println();
					GererUtilisateur.ajouter();
					break;
				case 2: 
					System.out.println();
					System.out.println(MOD_TITLE);
					System.out.println();
					GererUtilisateur.modifier();
					break;
				case 3: 
					System.out.println();
					System.out.println(DEL_TITLE);
					System.out.println();
					GererUtilisateur.supprimer();
					break;
				case 4: 
					break;
				}break;
				
			case 3:
				switch(MenuSeance.main()){
				
				case 1:
					GererSeance.ajouter();
					break;
				case 2: 
					GererSeance.modifier();
					break;
				case 3: 
					GererSeance.supprimer();
					break;
				case 4: 
					GererSeance.consulter();
					break;
				case 5: 
					break;
				}break;
				
			case 4:
				GererInscription.inscrire();
				break;
			case 5:
				GererConfirmation.confirmer();
				break;
			case 6:
				System.out.println();
				System.out.println("Procedure comptable non implemente");
				System.out.println();
				System.out.println("Appuyer sur entre pour continuer");
				Utils.lireData();
				break;
				
			case 7:
				System.exit(0);
				break;
			}
		}
	}
}
