public class Menu { // contient les methodes pour valider les donnes
	private static final int CLEAR_LINES = 10;
	
	protected static final String OPTION_PROMPT_MSG = "Veuillez choisir une option parmi les suivantes :";
	protected static final String CHOICE_MSG = "Votre choix : ";
	protected static final String INVALID_MSG = "Choix invalide, veuillez reessayer. \n";
	protected static final String ABONNEMENT_MSG = "Le membre a-t-il paye l'abonnement? (O/N) : ";
	protected static final String OUI = "O";
	protected static final String NON = "N";
	protected static final String BACK_MSG = " Q. Revenir au menu principal";
	protected static final String QUIT_MSG = " Q. Quitter";
	
	protected static final String USER_PROMPT = "Veuillez entrer le numero d'un utilisateur : ";
	protected static final String SEANCE_PROMPT ="Veuillez entrer le numero de la seance : ";
	
	public static void clearConsole() {for(int i = 0; i<= CLEAR_LINES;i++ ) {System.out.println();}}
	
	protected static boolean validON(String msg) {
		System.out.print(msg);
		boolean ret = false;
		for(;;) {
			String tmp = Utils.lireData();	
			if(tmp.toUpperCase().equals(OUI)) {ret = true;break;}
			else if(tmp.toUpperCase().equals(NON)) {break;}
			System.out.println(INVALID_MSG);
			System.out.print(msg);
		}
		return ret;
	}
	
	protected static String validNum(String msg, int length) {
		System.out.print(msg);
		String numero;
		for(;;) {
			 numero = Utils.lireData();
			 if(numero.length() == length) {
				 if(Utils.stringToInt(numero)!= -1) {break;}  //verifie qu'il n'y a pas de lettres dans le # de membre 
			 }
			 else {if(numero.toUpperCase().equals("Q")) {numero = "QUIT";break;}}
			 System.out.println(INVALID_MSG);
			 System.out.print(msg);
		}
		return numero;
	}
	
	protected static String prompt(String msg) {
		System.out.print(msg); 
		return Utils.lireData();
	}
}
