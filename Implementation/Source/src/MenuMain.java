

public class MenuMain extends Menu { // affiche le splash screen et affiche le menu principal
	private static final int MIN_OPTION_MAIN = 1;
	private static final int MAX_OPTION_MAIN = 7;
	
	private static final String WELCOME_MSG = "Bienvenue dans le centre de donnees \nAppuyer sur entre pour continuer";
	private static final String OPTION1_MSG = " 1. Acces au centre";
	private static final String OPTION2_MSG = " 2. Gerer le dossier d'un utilisateur";
	private static final String OPTION3_MSG = " 3. Gerer une seance de service";
	private static final String OPTION4_MSG = " 4. Inscription d'un membre a une seance de service";
	private static final String OPTION5_MSG = " 5. Confirmer l'inscription d'un membre a une seance de service";
	private static final String OPTION6_MSG = " 6. Initier la procedure comptable";  /* Non implemente */
	
	private static final String SPLASH = 
			"    _  _    _______     ____  __ \r\n" + "  _| || |_ / ____\\ \\   / /  \\/  |\r\n" + 
			" |_  __  _| |  __ \\ \\_/ /| \\  / |\r\n" + "  _| || |_| | |_ | \\   / | |\\/| |\r\n" + 
			" |_  __  _| |__| |  | |  | |  | |\r\n" + "   |_||_|  \\_____|  |_|  |_|  |_|\r\n";
	
	public static void splash() {
		System.out.println(SPLASH);
		System.out.println(WELCOME_MSG);
		Utils.lireData();
	}
	
	public static int afficher() {
		System.out.println();
		for(;;) {
			System.out.println(OPTION_PROMPT_MSG);
			System.out.println();
			System.out.println(OPTION1_MSG);
			System.out.println(OPTION2_MSG);
			System.out.println(OPTION3_MSG);
			System.out.println(OPTION4_MSG);
			System.out.println(OPTION5_MSG);
			System.out.println(OPTION6_MSG);
			System.out.println(QUIT_MSG);
			System.out.println();
			System.out.print(CHOICE_MSG);
			String in = Utils.lireData();
			int res = Utils.stringToInt(in);
			if(res >= MIN_OPTION_MAIN && res <= MAX_OPTION_MAIN ) {return res;}
			else {if (in.toUpperCase().equals("Q")){return 7;}
		    System.out.println(INVALID_MSG);}
		}
	}

}
