
public class MenuSeance extends Menu { // menu permettant de gerer les seance, ajouter modifier et supprimer
	private static final int MIN_OPTION_SEANCE = 1;
	private static final int MAX_OPTION_SEANCE = 5;
	
	private static final String TAB = " ";
	
	private static final String MANAGE_TITLE = "----Gerer une seance----";
	private static final String DEL_TITLE = "----Supprimer une seance----";
	private static final String OPTION1_MSG = " 1. Creer une seance";
	private static final String OPTION2_MSG = " 2. Modifier une seance";
	private static final String OPTION3_MSG = " 3. Supprimer une seance";
	private static final String OPTION4_MSG = " 4. Consulter les inscriptions a une seance";

	private static final String DATE_DEBUT_MSG = "Veuillez entrer la date de debut du service dans le format JJ-MM-AAAA : ";
	private static final String DATE_FIN_MSG = "Veuillez entrer la date de fin du service dans le format JJ-MM-AAAA : ";
	private static final String HEURE_MSG = "Veuillez entrer l'heure du service dans le format HH:MM : ";
	private static final String LUNDI_MSG = "Le service se donne-t-il le lundi? (O/N): ";
	private static final String MARDI_MSG = "Le service se donne-t-il le mardi? (O/N): ";
	private static final String MERCREDI_MSG = "Le service se donne-t-il le mercredi? (O/N): ";
	private static final String JEUDI_MSG = "Le service se donne-t-il le jeudi? (O/N): ";
	private static final String VENDREDI_MSG = "Le service se donne-t-il le vendredi? (O/N): ";
	private static final String SAMEDI_MSG = "Le service se donne-t-il le samedi? (O/N): ";
	private static final String DIMANCHE_MSG = "Le service se donne-t-il le dimanche? (O/N): ";
	private static final String CAPACITE_MSG = "Veuillez entrer la capacite maximale du service (max 30): ";
	private static final String FRAIS_DU_SERVICE_MSG = "Veuillez entrer les frais associe au service (max 100$): ";
	private static final String COMMENTAIRES_MSG = "Veuillez entrer les commentaires associes au service (max 100 caracteres): ";
	
	private static final String EDIT_DATE_DEBUT_PROMPT = "Voulez-vous modifier la date de debut du service ? (O/N) : ";
	private static final String EDIT_DATE_FIN_PROMPT = "Voulez-vous modifier la date de fin du service ? (O/N) : ";
	private static final String EDIT_HEURE_PROMPT = "Voulez-vous modifier l'heure du service ? (O/N) : ";
	private static final String EDIT_CAPACITE_PROMPT = "Voulez-vous modifier la capacite du service ? (O/N) : ";
	private static final String EDIT_FRAIS_DU_SERVICE_PROMPT = "Voulez-vous modifier les frais du services ? (O/N) : ";
	private static final String EDIT_COMMENTAIRES_PROMPT = "Voulez-vous modifier les commentaires ? (O/N) : ";
	private static final String EDIT_JOUR_PROMPT = "Voulez-vous modifier les jours de la semaine ou le service se donne ? (O/N) : ";
	
	private static final String FIELD_NUM_PROFESSIONNEL = "# professionnel :";
	private static final String FIELD_NUM_SCEANCE = "# seance : ";
	private static final String FIELD_DATE_DEBUT = "Date Debut : ";
	private static final String FIELD_DATE_FIN = "Date fin : ";
	private static final String FIELD_HEURE = "Heure du service : ";
	private static final String FIELD_CAPACITE = "Capacite : ";
	private static final String FIELD_FRAIS = "Frais de service : ";
	private static final String FIELD_COMMENTAIRES = "Commentaires : ";
	
	public static int main() { //propose les options du menuSeance
		for(;;) {
			System.out.println();
			System.out.println(MANAGE_TITLE);
			System.out.println();
			System.out.println(OPTION_PROMPT_MSG);
			System.out.println();
			System.out.println(OPTION1_MSG);
			System.out.println(OPTION2_MSG);
			System.out.println(OPTION3_MSG);
			System.out.println(OPTION4_MSG);
			System.out.println(BACK_MSG);
			System.out.println();
			System.out.print(CHOICE_MSG);
			String in = Utils.lireData();
			int res = Utils.stringToInt(in);
			if(res >= MIN_OPTION_SEANCE && res <= MAX_OPTION_SEANCE ) {return res;} //si une optione est selectionnee
			else {if (in.toUpperCase().equals("Q")){return 5;} //si on choisi de quitter
			System.out.println(INVALID_MSG);} //Sinon, un message d'erreur est affiche
		}
	}
	
	public static Seance ajouter(String id) {	//Sous-menu pour ajouter une seance	
		Seance a  = new Seance();
	
		a.setDateEtHeureActuelle(Utils.getDateHeure());
		a.setNumeroProfessionnel(id);
		
		System.out.print(DATE_DEBUT_MSG);	
		a.setDateDebut(Utils.lireData());
		
		System.out.print(DATE_FIN_MSG);
		a.setDateFin(Utils.lireData());
		
		System.out.print(HEURE_MSG);
		a.setHeure(Utils.lireData());
		a.setLundi(validON(LUNDI_MSG));
		a.setMardi(validON(MARDI_MSG));
		a.setMercredi(validON(MERCREDI_MSG));
		a.setJeudi(validON(JEUDI_MSG));
		a.setVendredi(validON(VENDREDI_MSG));
		a.setSamedi(validON(SAMEDI_MSG));
		a.setDimanche(validON(DIMANCHE_MSG));
		
		System.out.print(CAPACITE_MSG);
		a.setCapacite(Utils.lireData());
		
		System.out.print(FRAIS_DU_SERVICE_MSG);
		a.setFraisDeService(Utils.lireData());
		
		System.out.print(COMMENTAIRES_MSG);
		a.setCommentaires(Utils.lireData());
		
		return a;
	}
	
	public static Seance modifier(Seance mod) { //Sous-menu pour modifier une seance
		System.out.println(TAB+ FIELD_NUM_SCEANCE + mod.getNumero());
		System.out.println(TAB + FIELD_NUM_PROFESSIONNEL +mod.getNumeroProfessionnel());
		System.out.println(TAB + FIELD_DATE_DEBUT + mod.getDateDebut());
		System.out.println(TAB + FIELD_DATE_FIN + mod.getDateFin());
		System.out.println(TAB + FIELD_HEURE +mod.getHeure());
		System.out.println(TAB + FIELD_CAPACITE +mod.getCapacite());
		System.out.println(TAB + FIELD_FRAIS+ mod.getFraisDeService());
		System.out.println(TAB + FIELD_COMMENTAIRES+ mod.getCommentaires());
		if(mod.isLundi()) {System.out.println(TAB +"Lundi");}
		if(mod.isMardi()) {System.out.println(TAB +"Mardi");}
		if(mod.isMercredi()) {System.out.println(TAB +"Mercredi");}
		if(mod.isJeudi()) {System.out.println(TAB +"Jeudi");}
		if(mod.isVendredi()) {System.out.println(TAB +"Vendredi");}
		if(mod.isSamedi()) {System.out.println(TAB +"Samedi");}
		if(mod.isDimanche()) {System.out.println(TAB +"Dimanche");}
		System.out.println();

		if(validON(EDIT_DATE_DEBUT_PROMPT)) { mod.setDateDebut(prompt(DATE_DEBUT_MSG));}
			
		if(validON(EDIT_DATE_FIN_PROMPT)) { mod.setDateFin(prompt(DATE_FIN_MSG));}
		
		if(validON(EDIT_HEURE_PROMPT)) { mod.setHeure(prompt(HEURE_MSG));}
		
		if(validON(EDIT_CAPACITE_PROMPT)) { mod.setCapacite(prompt(CAPACITE_MSG));}
		
		if(validON(EDIT_FRAIS_DU_SERVICE_PROMPT)) { mod.setFraisDeService(prompt(FRAIS_DU_SERVICE_MSG));}
		
		if(validON(EDIT_COMMENTAIRES_PROMPT)) { mod.setCommentaires(prompt(COMMENTAIRES_MSG));}

		if(validON(EDIT_JOUR_PROMPT)) {
			mod.setLundi(validON(LUNDI_MSG));
			mod.setMardi(validON(MARDI_MSG));
			mod.setMercredi(validON(MERCREDI_MSG));
			mod.setJeudi(validON(JEUDI_MSG));
			mod.setVendredi(validON(VENDREDI_MSG));
			mod.setSamedi(validON(SAMEDI_MSG));
			mod.setDimanche(validON(DIMANCHE_MSG));}
		
		return mod;
	}
	public static void supprimer() { //Sous-menu pour supprimer une seance
		System.out.println();
		System.out.println(DEL_TITLE);
		System.out.println();
	}
}
