
public class MenuUtilisateur extends Menu {
	
	private static final int MIN_OPTION_UTILISATEUR = 1;
	private static final int MAX_OPTION_UTILISATEUR = 4;
	
	protected static final int UTILISATEUR_LENGTH = 9;
	
	private static final String TAB = " "; 
	
	private static final String MANAGE_TITLE = "----Gerer le dossier d'un utilisateur----";
	private static final String OPTION1_MSG = " 1. Ajouter un utilisateur";
	private static final String OPTION2_MSG = " 2. Modifier le dossier d'un utilisateur";
	private static final String OPTION3_MSG = " 3. Supprimer le dossier d'un utilisateur";
	
	
	private static final String PRENOM_MSG = "Veuillez entrer le prenom de l'utilisateur : ";
	private static final String NOM_MSG = "Veuillez entrer le nom de l'utilisateur : ";
	
	private static final String EDIT_NOM_PROMPT = "Voulez-vous modifier le nom de l'utilisateur ? (O/N) : ";
	private static final String EDIT_PRENOM_PROMPT = "Voulez-vous modifier le prenom de l'utilisateur ? (O/N) : ";
	private static final String EDIT_PAYE_PROMPT = "Voulez-vous modifier le payment de l'utilisateur? (O/N) : ";
	private static final String PROFESSIONNEL_MSG = "L'utilisateur est-il un professionnel ? (O/N) : ";
	
	private static final String EDIT_PROFESSIONNEL_PROMPT = "Voulez-vous modifier le statut de l'utilisateur?";
	
	
	public static int main() {	
		for(;;) {
			System.out.println();
			System.out.println(MANAGE_TITLE);
			System.out.println();
			System.out.println(OPTION_PROMPT_MSG);
			System.out.println();
			System.out.println(OPTION1_MSG);
			System.out.println(OPTION2_MSG);
			System.out.println(OPTION3_MSG);
			System.out.println(BACK_MSG);
			System.out.println();
			System.out.print(CHOICE_MSG);
			String in = Utils.lireData();
			int res = Utils.stringToInt(in);
			if(res >= MIN_OPTION_UTILISATEUR && res <= MAX_OPTION_UTILISATEUR ) {return res;}
			else {if (in.toUpperCase().equals("Q")){return 4;}
			System.out.println(INVALID_MSG);}
		}
	}
	
	public static Utilisateur ajouter() {
		Utilisateur a  = new Utilisateur();
		System.out.print(PRENOM_MSG);
		a.setPrenom(Utils.lireData());
		
		System.out.print(NOM_MSG);
		a.setNom(Utils.lireData());
		
		a.setProfessionnel(validON(PROFESSIONNEL_MSG));      
		if(a.isProfessionnel()) { a.setPaye(true);}			//On ne paie pas si on est professionnel
		else {a.setPaye(validON(ABONNEMENT_MSG));}
		
		return a;
	}
	
	public static Utilisateur modifier(Utilisateur mod) {
		System.out.println(TAB + mod.getNumero());
		System.out.println(TAB + mod.getPrenom());
		System.out.println(TAB + mod.getNom());
		if(mod.isPaye()){System.out.println(TAB+ "Frais acquitte " );}
		else {System.out.println(TAB+ "Frais non acquitte " );}
		if(mod.isProfessionnel()){System.out.println(TAB+ "Professionnel" );}
		else {System.out.println(TAB+ "Membre" );}
		System.out.println();
		
		if(validON(EDIT_PRENOM_PROMPT)) { mod.setPrenom(prompt(PRENOM_MSG));}
		if(validON(EDIT_NOM_PROMPT)) { mod.setNom(prompt(NOM_MSG));}
		if(validON(EDIT_PROFESSIONNEL_PROMPT)) {mod.setProfessionnel(validON(PROFESSIONNEL_MSG));}
		if(mod.isProfessionnel()) { mod.setPaye(true);
		} else {
			if(validON(EDIT_PAYE_PROMPT)) {
				mod.setPaye(validON(ABONNEMENT_MSG));
			}
		}
		
		return mod;
	}
}
