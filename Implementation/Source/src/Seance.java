
public class Seance extends Enregistrement implements Comparable<Seance> {
	protected static final String DELIMITER = "~";
	protected static final String LINE = "\r\n";
	
	
	private String numero;
	private String numeroProfessionnel;
	private String dateEtHeureActuelle;
	private String dateDebut;
	private String dateFin;
	private String heure;
	private String capacite;
	private String fraisDeService;
	private String commentaires;
	private boolean lundi;
	private boolean mardi;
	private boolean mercredi;
	private boolean jeudi;
	private boolean vendredi;
	private boolean samedi;
	private boolean dimanche;
	
	
	@Override
	public String toString() {
		return numero + DELIMITER + numeroProfessionnel + DELIMITER + dateEtHeureActuelle + 
				DELIMITER + dateDebut + DELIMITER+ dateFin + DELIMITER+ heure+ DELIMITER+ capacite
				+ DELIMITER+ fraisDeService + DELIMITER+ commentaires + DELIMITER+ lundi+ DELIMITER+ mardi
				+ DELIMITER+ mercredi+ DELIMITER+ jeudi+ DELIMITER+ vendredi + DELIMITER+ samedi+ DELIMITER+ dimanche ;
	}
	
	public String afficherInfo(Boolean professionnel) {
		String ret = "";
		ret+=  "numero de seance : " + numero+ LINE ;
		if(professionnel) {ret+=  "numero du professionnel : " + numeroProfessionnel + LINE;}
		
		ret+= recurence() + LINE;
		ret+= "service offer du "+ dateDebut +" au "+ dateFin +LINE;
		
		
		ret+= "Capacite : " + capacite + LINE;
		
		ret+= "Frais de service : " + fraisDeService + LINE;
		
		ret+= "Commentaire : " + commentaires + LINE;
		return  ret;
	}
	
	public String recurence() {
		String ret="Recurence du service : ";
		if(lundi) {ret+= "lundi ";}
		if(mardi) {ret+= "mardi ";}
		if(mercredi) {ret+= "mercredi ";}
		if(jeudi) {ret+= "jeudi ";}
		if(vendredi) {ret+= "vendredi ";}
		if(samedi) {ret+= "samedi ";}
		if(dimanche) {ret+= "dimanche ";}
		return ret;
	}


	public String getNumero() {
		return numero;
	}


	public void setNumero(String numero) {
		this.numero = numero;
	}


	public String getNumeroProfessionnel() {
		return numeroProfessionnel;
	}


	public void setNumeroProfessionnel(String numeroProfessionnel) {
		this.numeroProfessionnel = numeroProfessionnel;
	}


	public String getDateEtHeureActuelle() {
		return dateEtHeureActuelle;
	}


	public void setDateEtHeureActuelle(String dateEtHeureActuelle) {
		this.dateEtHeureActuelle = dateEtHeureActuelle;
	}


	public String getDateDebut() {
		return dateDebut;
	}


	public void setDateDebut(String dateDebut) {
		this.dateDebut = dateDebut;
	}


	public String getDateFin() {
		return dateFin;
	}


	public void setDateFin(String dateFin) {
		this.dateFin = dateFin;
	}


	public String getHeure() {
		return heure;
	}


	public void setHeure(String heure) {
		this.heure = heure;
	}


	public String getCapacite() {
		return capacite;
	}


	public void setCapacite(String capacite) {
		this.capacite = capacite;
	}


	public String getFraisDeService() {
		return fraisDeService;
	}


	public void setFraisDeService(String fraisDeService) {
		this.fraisDeService = fraisDeService;
	}


	public String getCommentaires() {
		return commentaires;
	}


	public void setCommentaires(String commentaires) {
		this.commentaires = commentaires;
	}


	public boolean isLundi() {
		return lundi;
	}


	public void setLundi(boolean lundi) {
		this.lundi = lundi;
	}


	public boolean isMardi() {
		return mardi;
	}


	public void setMardi(boolean mardi) {
		this.mardi = mardi;
	}


	public boolean isMercredi() {
		return mercredi;
	}


	public void setMercredi(boolean mercredi) {
		this.mercredi = mercredi;
	}


	public boolean isJeudi() {
		return jeudi;
	}


	public void setJeudi(boolean jeudi) {
		this.jeudi = jeudi;
	}


	public boolean isVendredi() {
		return vendredi;
	}


	public void setVendredi(boolean vendredi) {
		this.vendredi = vendredi;
	}


	public boolean isSamedi() {
		return samedi;
	}


	public void setSamedi(boolean samedi) {
		this.samedi = samedi;
	}


	public boolean isDimanche() {
		return dimanche;
	}


	public void setDimanche(boolean dimanche) {
		this.dimanche = dimanche;
	}

	@Override
    public int compareTo(Seance s) {
        return this.numero.compareTo(s.numero);
    }
	
	@Override
    public boolean equals(Object o) { 
		String s = (String) o;
        return numero.equals(s);
    }

	
	
	
}
