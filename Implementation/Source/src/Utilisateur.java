
public class Utilisateur extends Enregistrement implements Comparable<Utilisateur> { 
	private static final char DELIMITER = '~';
	
	
	private String numero;
	private String prenom;
	private String nom;
	private boolean paye;
	private boolean professionnel;
	
	@Override
	public String toString() {
		return numero + DELIMITER + prenom + DELIMITER + nom + DELIMITER + Boolean.toString(paye)+ DELIMITER+ Boolean.toString(professionnel)+ DELIMITER;
	}
	
	public String testPrint() {
		return "\n" + numero + "\n" + prenom + "\n" + nom + "\n" + paye;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public boolean isPaye() {
		return paye;
	}

	public void setPaye(boolean paye) {
		this.paye = paye;
	}
	
	public boolean isProfessionnel() {
		return professionnel;
	}

	public void setProfessionnel(boolean professionnel) {
		this.professionnel = professionnel;
	}
	
	@Override
    public int compareTo(Utilisateur u) {
        return this.numero.compareTo(u.numero);
    }
	
	@Override
    public boolean equals(Object o) { 
		String s = (String) o;
        return numero.equals(s);
    }	
}
