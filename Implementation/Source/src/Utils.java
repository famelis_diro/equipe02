import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;

public final class Utils { //utilites generales utilise un peu partout dans le programme
	private static final String FORMAT_DATE_HEURE =  "dd-MM-yyyy HH:mm:ss";
	protected static final String DELIMITER = "~";
	protected static final String LINE = "\r\n";
	
	static Scanner scanner;
	static BufferedReader br;
	
	public static int stringToInt(String in){
		int out=-1;
		try {out = Integer.parseInt(in);}
		catch(NumberFormatException e) {}
		return out;
	}
	
	protected static String addZeros(int i, int length) {
		String ret = String.valueOf(i);
		while(ret.length() != length) {ret = "0" + ret;}
		return ret;
	}
	
	public static void ouvrirScanner() {scanner = new Scanner(System.in);}

	public static String lireData() {
		String ret = scanner.nextLine();
		if(ret.equals("")) {ret="-";}
		return ret;
	}
	
	public static void fermerScanner() {scanner.close();}
	public static void fermerBufferedReader() {
		try {br.close();} 
		catch (IOException e) {}}
	
	public static String lireFichier(String f) {
		String text = "";
		try {
			br = new BufferedReader(new FileReader(f));
			String line = br.readLine(); 
	  
			while (line != null) {
				text += line + LINE;   /*\r\n pour faire un saut de ligne*/
				line = br.readLine();
	     }
		} catch (FileNotFoundException e) {
			ecrireFichier("", f );
			return text;
		} catch (IOException e) {}
		return text;
	}

	public static void wipeFichier(String f) {ecrireFichier("", f );}
	
	public static void ecrireFichier(String in, String f ) {
		
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
				  new FileOutputStream(f), "utf-8"))) {writer.write(in);}
		catch(IOException ex) {}
	}
	
	public static String getDateHeure() {
		DateFormat dateFormat = new SimpleDateFormat(FORMAT_DATE_HEURE);
		Date date = new Date();
		return dateFormat.format(date).toString();
	}
	
	public static String[][] fichier2Tableau(String fichier){
		String fichierString = Utils.lireFichier(fichier);
		if(fichierString == "") {return null;} //a changer
		String[] tab1 = fichierString.split(LINE);
		int length = tab1.length;
		String[][]tab2= new String[length][];
		
		for(int i = 0; i < length;i++) {tab2[i] = tab1[i].split(DELIMITER);}
		return tab2;
	}
	
	public static void ecrireChose(String input, String fichier) {
		String tmp = Utils.lireFichier(fichier);
		Utils.ecrireFichier(tmp + input, fichier);
	}
	
	public static int chercherChose(String id, Object[] tableau) {
		int index =-1;
		for(int i=0; i<tableau.length;i++) {
			if(tableau[i].equals(id)) {index = i;break;}
		}
		return index;
	}
	
	public static void ecrireTableauChose(Object[] o, String file, Boolean sort) {
		String write="";
		if(sort) {Arrays.sort(o);}
		for(Object i : o) {write+=i.toString()+LINE;}
		Utils.ecrireFichier(write, file);
	}
	
	public static String genererNumeroEnregistrement(Enregistrement[] c, int length) {
		return Utils.addZeros(Integer.parseInt(c[c.length-1].getNumero())+1,length);
		
	}

	public static Enregistrement[] retirerChose(int index,Enregistrement[] c) {
		int length = c.length-1;
		c[index] = c[length];
		Enregistrement[] ret = Arrays.copyOfRange(c, 0, length);
		return ret;
	}
	
	
}
